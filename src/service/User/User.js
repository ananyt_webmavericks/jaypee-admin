import axios from 'axios';

export class User {

    getUsers() {
        return axios.get('assets/demo/data/Users/Users.json').then(res => res.data.data);
    }
}
import axios from 'axios';

export class Alumnipic {

    getAlumniMeet() {
        return axios.get('assets/demo/data/Alumni/AlumniMeet.json').then(res => res.data.data);
    }
    getAlumniConvo() {
        return axios.get('assets/demo/data/Alumni/AlumniConvo.json').then(res => res.data.data);
    }
    getAlumniCarnival() {
        return axios.get('assets/demo/data/Alumni/AlumniCarnival.json').then(res => res.data.data);
    }


}
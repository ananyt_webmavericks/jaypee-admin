import axios from 'axios';

export class Faculty {

    getFacultyStaff() {
        return axios.get('assets/demo/data/Academics/Faculty.json').then(res => res.data.data);
    }
    getFaculty() {
        return axios.get('assets/demo/data/Academics/Faculty-staff-small.json').then(res => res.data.data);
    }

}
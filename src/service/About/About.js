import axios from 'axios';

export class About {

    getGeneral() {
        return axios.get('assets/demo/data/About/General.json').then(res => res.data.data);
    }
    getAccount() {
        return axios.get('assets/demo/data/About/Account.json').then(res => res.data.data);
    }
    getConsultant() {
        return axios.get('assets/demo/data/About/Consultant.json').then(res => res.data.data);
    }
    getLrc() {
        return axios.get('assets/demo/data/About/Lrc.json').then(res => res.data.data);
    }
    getTechnician() {
        return axios.get('assets/demo/data/About/Technician.json').then(res => res.data.data);
    }
   
    

}
import axios from 'axios';

export class Highlights {

    getHighlights() {
        return axios.get('assets/demo/data/Home/Highlight.json').then(res => res.data.data);
    }
   
    

}
import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Alumnipic } from '../../service/Alumni/Alumnipic';


const AlumniCarnival = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyEvents = {
        id: null,
        carnivalId: null,
        heading: '',
        carnival: '',
    };

    const [carnivalImg, setCarnivalImg] = useState(null);
    const [carnivalDialog, setCarnivalDialog] = useState(false);
    const [deleteCarnivalDialog, setDeleteCarnivalDialog] = useState(false);
    const [deleteCarnivalImageDialog, setDeleteCarnivalImageDialog] = useState(false);
    const [carnivalImage, setCarnivalImage] = useState(emptyEvents);
    const [selectedCarnivalImg, setSelectedCarnivalImg] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const alumnipic = new Alumnipic();
        alumnipic.getAlumniCarnival().then(data => setCarnivalImg(data));
    }, []);


    const openNew = () => {
        setCarnivalImage(emptyEvents);
        setSubmitted(false);
        setCarnivalDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setCarnivalDialog(false);
    }

    const hidedeleteCarnivalDialog = () => {
        setDeleteCarnivalDialog(false);
    }

    const hidedeleteCarnivalImageDialog = () => {
        setDeleteCarnivalImageDialog(false);
    }

    const savecarnivalImage = () => {
        setSubmitted(true);

        if (carnivalImage.name.trim()) {
            let _carnivalImg = [...carnivalImg];
            let _carnivalImage = { ...carnivalImage };
            if (carnivalImage.id) {
                const index = findIndexById(carnivalImage.id);

                _carnivalImg[index] = _carnivalImage;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'carnivalImage Updated', life: 3000 });
            }
            else {
                _carnivalImage.id = createId();
                _carnivalImage.image = 'carnivalImage-placeholder.svg';
                _carnivalImg.push(_carnivalImage);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'carnivalImage Created', life: 3000 });
            }

            setCarnivalImg(_carnivalImg);
            setCarnivalDialog(false);
            setCarnivalImage(emptyEvents);
        }
    }

    const editcarnivalImage = (carnivalImage) => {
        setCarnivalImage({ ...carnivalImage });
        setCarnivalDialog(true);
    }

    const confirmDeletecarnivalImage = (carnivalImage) => {
        setCarnivalImage(carnivalImage);
        setDeleteCarnivalDialog(true);
    }

    const deletecarnivalImage = () => {
        let _carnivalImg = carnivalImg.filter(val => val.id !== carnivalImage.id);
        setCarnivalImg(_carnivalImg);
        setDeleteCarnivalDialog(false);
        setCarnivalImage(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'carnivalImage Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < carnivalImg.length; i++) {
            if (carnivalImg[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteCarnivalImageDialog(true);
    }

    const deleteselectedCarnivalImg = () => {
        let _carnivalImg = carnivalImg.filter(val => !selectedCarnivalImg.includes(val));
        setCarnivalImg(_carnivalImg);
        setDeleteCarnivalImageDialog(false);
        setSelectedCarnivalImg(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'carnivalImg Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _carnivalImage = { ...carnivalImage };
        _carnivalImage[`${name}`] = val;
        setCarnivalImage(_carnivalImage);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedCarnivalImg || !selectedCarnivalImg.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    const categoryBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image category</span>
                {rowData.heading}
            </>
        );
    }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/Alumni/${rowData.carnival}`} alt={rowData.carnival} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editcarnivalImage(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletecarnivalImage(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Alumni Carnival Photos</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const carnivalDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savecarnivalImage} />
        </>
    );
    const deleteCarnivalDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteCarnivalDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletecarnivalImage} />
        </>
    );
    const deleteCarnivalImageDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteCarnivalImageDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedCarnivalImg} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={carnivalImg} selection={selectedCarnivalImg} onSelectionChange={(e) => setSelectedCarnivalImg(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} carnivalImg"
                        globalFilter={globalFilter} emptyMessage="No carnivalImg found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={carnivalDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={carnivalDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteCarnivalDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCarnivalDialogFooter} onHide={hidedeleteCarnivalDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {carnivalImage && <span>Are you sure you want to delete <b>{carnivalImage.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteCarnivalImageDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCarnivalImageDialogFooter} onHide={hidedeleteCarnivalImageDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {carnivalImage && <span>Are you sure you want to delete the selected carnivalImg?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(AlumniCarnival, comparisonFn);
import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Alumnipic } from '../../service/Alumni/Alumnipic';


const AlumniMeet = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyEvents = {
        id: null,
        meetid: null,
        heading: '',
        meet: '',
    };

    const [meetImg, setMeetImg] = useState(null);
    const [meetDialog, setMeetDialog] = useState(false);
    const [deleteMeetDialog, setDeleteMeetDialog] = useState(false);
    const [deleteConvoImageDialog, setDeleteConvoImageDialog] = useState(false);
    const [convoImage, setConvoImage] = useState(emptyEvents);
    const [selectedMeetImg, setSelectedMeetImg] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const alumnipic = new Alumnipic();
        alumnipic.getAlumniMeet().then(data => setMeetImg(data));
    }, []);

    const openNew = () => {
        setConvoImage(emptyEvents);
        setSubmitted(false);
        setMeetDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setMeetDialog(false);
    }

    const hidedeleteMeetDialog = () => {
        setDeleteMeetDialog(false);
    }

    const hidedeleteConvoImageDialog = () => {
        setDeleteConvoImageDialog(false);
    }

    const saveconvoImage = () => {
        setSubmitted(true);

        if (convoImage.name.trim()) {
            let _meetImg = [...meetImg];
            let _convoImage = { ...convoImage };
            if (convoImage.id) {
                const index = findIndexById(convoImage.id);

                _meetImg[index] = _convoImage;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImage Updated', life: 3000 });
            }
            else {
                _convoImage.id = createId();
                _convoImage.image = 'convoImage-placeholder.svg';
                _meetImg.push(_convoImage);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImage Created', life: 3000 });
            }

            setMeetImg(_meetImg);
            setMeetDialog(false);
            setConvoImage(emptyEvents);
        }
    }

    const editconvoImage = (convoImage) => {
        setConvoImage({ ...convoImage });
        setMeetDialog(true);
    }

    const confirmDeleteconvoImage = (convoImage) => {
        setConvoImage(convoImage);
        setDeleteMeetDialog(true);
    }

    const deleteconvoImage = () => {
        let _meetImg = meetImg.filter(val => val.id !== convoImage.id);
        setMeetImg(_meetImg);
        setDeleteMeetDialog(false);
        setConvoImage(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImage Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < meetImg.length; i++) {
            if (meetImg[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteConvoImageDialog(true);
    }

    const deleteselectedMeetImg = () => {
        let _meetImg = meetImg.filter(val => !selectedMeetImg.includes(val));
        setMeetImg(_meetImg);
        setDeleteConvoImageDialog(false);
        setSelectedMeetImg(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'meetImg Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _convoImage = { ...convoImage };
        _convoImage[`${name}`] = val;
        setConvoImage(_convoImage);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedMeetImg || !selectedMeetImg.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    const categoryBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image category</span>
                {rowData.heading}
            </>
        );
    }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/Alumni/${rowData.meet}`} alt={rowData.meet} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editconvoImage(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteconvoImage(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Alumni Meet Photos</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const meetDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveconvoImage} />
        </>
    );
    const deleteMeetDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteMeetDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteconvoImage} />
        </>
    );
    const deleteConvoImageDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteConvoImageDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedMeetImg} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={meetImg} selection={selectedMeetImg} onSelectionChange={(e) => setSelectedMeetImg(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} meetImg"
                        globalFilter={globalFilter} emptyMessage="No meetImg found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={meetDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={meetDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteMeetDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteMeetDialogFooter} onHide={hidedeleteMeetDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {convoImage && <span>Are you sure you want to delete <b>{convoImage.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteConvoImageDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteConvoImageDialogFooter} onHide={hidedeleteConvoImageDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {convoImage && <span>Are you sure you want to delete the selected meetImg?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(AlumniMeet, comparisonFn);
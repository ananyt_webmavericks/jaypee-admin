import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Alumnipic } from '../../service/Alumni/Alumnipic';


const AlumniConvo = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };


    let emptyEvents = {
        id: null,
        convoId: null,
        heading: '',
        convo: '',
    };

    const [convoImg, setConvoImg] = useState(null);
    const [convoDialog, setConvoDialog] = useState(false);
    const [deleteConvoDialog, setDeleteConvoDialog] = useState(false);
    const [deleteConvoImageDialog, setDeleteConvoImageDialog] = useState(false);
    const [convoImage, setConvoImage] = useState(emptyEvents);
    const [selectedConvoImg, setSelectedConvoImg] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const alumnipic = new Alumnipic();
        alumnipic.getAlumniConvo().then(data => setConvoImg(data));
    }, []);


    const openNew = () => {
        setConvoImage(emptyEvents);
        setSubmitted(false);
        setConvoDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setConvoDialog(false);
    }

    const hidedeleteConvoDialog = () => {
        setDeleteConvoDialog(false);
    }

    const hidedeleteConvoImageDialog = () => {
        setDeleteConvoImageDialog(false);
    }

    const saveconvoImage = () => {
        setSubmitted(true);

        if (convoImage.name.trim()) {
            let _convoImg = [...convoImg];
            let _convoImage = { ...convoImage };
            if (convoImage.id) {
                const index = findIndexById(convoImage.id);

                _convoImg[index] = _convoImage;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImage Updated', life: 3000 });
            }
            else {
                _convoImage.id = createId();
                _convoImage.image = 'convoImage-placeholder.svg';
                _convoImg.push(_convoImage);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImage Created', life: 3000 });
            }

            setConvoImg(_convoImg);
            setConvoDialog(false);
            setConvoImage(emptyEvents);
        }
    }

    const editconvoImage = (convoImage) => {
        setConvoImage({ ...convoImage });
        setConvoDialog(true);
    }

    const confirmDeleteconvoImage = (convoImage) => {
        setConvoImage(convoImage);
        setDeleteConvoDialog(true);
    }

    const deleteconvoImage = () => {
        let _convoImg = convoImg.filter(val => val.id !== convoImage.id);
        setConvoImg(_convoImg);
        setDeleteConvoDialog(false);
        setConvoImage(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImage Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < convoImg.length; i++) {
            if (convoImg[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteConvoImageDialog(true);
    }

    const deleteselectedConvoImg = () => {
        let _convoImg = convoImg.filter(val => !selectedConvoImg.includes(val));
        setConvoImg(_convoImg);
        setDeleteConvoImageDialog(false);
        setSelectedConvoImg(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'convoImg Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _convoImage = { ...convoImage };
        _convoImage[`${name}`] = val;
        setConvoImage(_convoImage);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedConvoImg || !selectedConvoImg.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    const categoryBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image category</span>
                {rowData.heading}
            </>
        );
    }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/Alumni/${rowData.convo}`} alt={rowData.convo} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editconvoImage(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteconvoImage(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Alumni Convocation Photos</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const convoDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveconvoImage} />
        </>
    );
    const deleteConvoDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteConvoDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteconvoImage} />
        </>
    );
    const deleteConvoImageDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteConvoImageDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedConvoImg} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={convoImg} selection={selectedConvoImg} onSelectionChange={(e) => setSelectedConvoImg(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} convoImg"
                        globalFilter={globalFilter} emptyMessage="No convoImg found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={convoDialog} style={{ width: '450px' }} header="Convocation Picture" modal className="p-fluid" footer={convoDialogFooter} onHide={hideDialog}>
                        <div className="field">

                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />

                        </div>

                    </Dialog>

                    <Dialog visible={deleteConvoDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteConvoDialogFooter} onHide={hidedeleteConvoDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {convoImage && <span>Are you sure you want to delete <b>{convoImage.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteConvoImageDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteConvoImageDialogFooter} onHide={hidedeleteConvoImageDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {convoImage && <span>Are you sure you want to delete the selected Entry?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(AlumniConvo, comparisonFn);
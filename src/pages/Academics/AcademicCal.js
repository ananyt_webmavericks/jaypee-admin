import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { UpcomingEvent } from '../../service/Home/Events';
import { Academics } from '../../service/Academics/Academic';



const AcademicCal = () => {


    let emptyacademicList = {
        id: null,
        listId:null,
        fileName: '',
        fileLoc: '',
    };

    const [academic, setAcademic] = useState(null);
    const [academicsDialog, setAcademicsDialog] = useState(false);
    const [deleteAcademicsDialog, setDeleteAcademicsDialog] = useState(false);
    const [deleteAcademicDialog, setDeleteAcademicDialog] = useState(false);
    const [academicList, setAcademicList] = useState(emptyacademicList);
    const [selectedAcademic, setSelectedAcademic] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const academics = new Academics();
        academics.getCalender().then(data => setAcademic(data));
    }, []);

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setAcademicList(emptyacademicList);
        setSubmitted(false);
        setAcademicsDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setAcademicsDialog(false);
    }

    const hidedeleteAcademicsDialog = () => {
        setDeleteAcademicsDialog(false);
    }

    const hidedeleteAcademicDialog = () => {
        setDeleteAcademicDialog(false);
    }

    const saveacademicList = () => {
        setSubmitted(true);

        if (academicList.name.trim()) {
            let _academic = [...academic];
            let _academicList = { ...academicList };
            if (academicList.id) {
                const index = findIndexById(academicList.id);

                _academic[index] = _academicList;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'academicList Updated', life: 3000 });
            }
            else {
                _academicList.id = createId();
                _academicList.image = 'academicList-placeholder.svg';
                _academic.push(_academicList);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'academicList Created', life: 3000 });
            }

            setAcademic(_academic);
            setAcademicsDialog(false);
            setAcademicList(emptyacademicList);
        }
    }

    const editacademicList = (academicList) => {
        setAcademicList({ ...academicList });
        setAcademicsDialog(true);
    }

    const confirmDeleteacademicList = (academicList) => {
        setAcademicList(academicList);
        setDeleteAcademicsDialog(true);
    }

    const deleteacademicList = () => {
        let _academic = academic.filter(val => val.id !== academicList.id);
        setAcademic(_academic);
        setDeleteAcademicsDialog(false);
        setAcademicList(emptyacademicList);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'academicList Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < academic.length; i++) {
            if (academic[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteAcademicDialog(true);
    }

    const deleteselectedAcademic = () => {
        let _academic = academic.filter(val => !selectedAcademic.includes(val));
        setAcademic(_academic);
        setDeleteAcademicDialog(false);
        setSelectedAcademic(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'academic Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _academicList = { ...academicList };
        _academicList[`${name}`] = val;
        setAcademicList(_academicList);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedAcademic || !selectedAcademic.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Announcements</span>
                {rowData.fileName}
            </>
        );
    }

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">LinkTo</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }

    const priceBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">LinkTo</span>
                {formatCurrency(rowData.fileLoc)}
            </>
        );
    }

    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Department</span>
    //             {rowData.dpmt}
    //         </>
    //     );
    // }

    // const ratingBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Readmore</span>
    //             <Rating value={rowData.readmore} readonly cancel={false} />
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editacademicList(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteacademicList(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Academic Calender</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const academicsDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveacademicList} />
        </>
    );
    const deleteAcademicsDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteAcademicsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteacademicList} />
        </>
    );
    const deleteAcademicDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteAcademicDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedAcademic} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={academic} selection={selectedAcademic} onSelectionChange={(e) => setSelectedAcademic(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} academic"
                        globalFilter={globalFilter} emptyMessage="No academic found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Academic cal" header="Academic cal" sortable body={nameBodyTemplate} headerStyle={{ width: '30%', minWidth: '10rem' }}></Column>
                        <Column field="Pdf" header="Pdf" body={priceBodyTemplate} style={{ width: "40%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate} style={{ float: "right",  paddingBottom: "22px" ,width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={academicsDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={academicsDialogFooter} onHide={hideDialog}>
                    <div className="field">
                    <FileUpload  mode = "basic" accept="pdf/*"  label="Upload File" chooseLabel="Upload File" className="mr-2 inline-block"/>
 
                    </div>
                        <div className="field">
                            <label htmlFor="name">Announcement</label>
                            <InputTextarea id="announcement" value={academicList.announcement} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !academicList.announcement })} />
                            {submitted && !academicList.name && <small className="p-invalid">Announcement is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">link To</label>
                            <InputText id="linkTo" value={academicList.linkTo} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !academicList.linkTo })} />

                        </div>

                    </Dialog>

                    <Dialog visible={deleteAcademicsDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteAcademicsDialogFooter} onHide={hidedeleteAcademicsDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {academicList && <span>Are you sure you want to delete <b>{academicList.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteAcademicDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteAcademicDialogFooter} onHide={hidedeleteAcademicDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {academicList && <span>Are you sure you want to delete the selected academic?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(AcademicCal, comparisonFn);
import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import classNames from 'classnames';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Training } from '../../service/Training/Training';


const TrainingCell = () => {

    let emptyEvents = {
        id:null,
        beplacementno: null,
        beplacementname:'',
    };

    const [placed, setPlaced] = useState(null);
    const [placedDialog, setPlacedDialog] = useState(false);
    const [deletePlacedDialog, setDeletePlacedDialog] = useState(false);
    const [deleteTrainingDialog, setDeleteTrainingDialog] = useState(false);
    const [training, setTraining] = useState(emptyEvents);
    const [selectedPlaced, setSelectedPlaced] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const training = new Training();
        training.getPlacementCell().then(data => setPlaced(data));
    }, []);


    const openNew = () => {
        setTraining(emptyEvents);
        setSubmitted(false);
        setPlacedDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setPlacedDialog(false);
    }

    const hidedeletePlacedDialog = () => {
        setDeletePlacedDialog(false);
    }

    const hidedeleteTrainingDialog = () => {
        setDeleteTrainingDialog(false);
    }

    const savetraining = () => {
        setSubmitted(true);

        if (training.name.trim()) {
            let _placed = [...placed];
            let _training = { ...training };
            if (training.id) {
                const index = findIndexById(training.id);

                _placed[index] = _training;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'training Updated', life: 3000 });
            }
            else {
                _training.id = createId();
                _training.image = 'training-placeholder.svg';
                _placed.push(_training);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'training Created', life: 3000 });
            }

            setPlaced(_placed);
            setPlacedDialog(false);
            setTraining(emptyEvents);
        }
    }

    const edittraining = (training) => {
        setTraining({ ...training });
        setPlacedDialog(true);
    }

    const confirmDeletetraining = (training) => {
        setTraining(training);
        setDeletePlacedDialog(true);
    }

    const deletetraining = () => {
        let _placed = placed.filter(val => val.id !== training.id);
        setPlaced(_placed);
        setDeletePlacedDialog(false);
        setTraining(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'training Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < placed.length; i++) {
            if (placed[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteTrainingDialog(true);
    }

    const deleteselectedPlaced = () => {
        let _placed = placed.filter(val => !selectedPlaced.includes(val));
        setPlaced(_placed);
        setDeleteTrainingDialog(false);
        setSelectedPlaced(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'placed Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _training = { ...training };
        _training[`${name}`] = val;
        setTraining(_training);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedPlaced || !selectedPlaced.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    const categoryBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                {rowData.beplacementname}
            </>
        );
    }

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             <img src={`assets/demo/images/training/${rowData.stulistimg}`} alt={rowData.stulistimg} className="shadow-2" width="200" />
    //         </>
    //     )
    // }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => edittraining(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletetraining(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Training and Placement Cell</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const placedDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savetraining} />
        </>
    );
    const deletePlacedDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeletePlacedDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletetraining} />
        </>
    );
    const deleteTrainingDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteTrainingDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedPlaced} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={placed} selection={selectedPlaced} onSelectionChange={(e) => setSelectedPlaced(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalplaceds} placed"
                        globalFilter={globalFilter} emptyMessage="No placed found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column> */}
                        <Column field="Recruiters"  header="Recruiters" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={placedDialog} style={{ width: '450px' }} header="Cell" modal className="p-fluid" footer={placedDialogFooter} onHide={hideDialog}>
                        {/* <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div> */}
                        <div className="field">
                            <label htmlFor="title">Recruiter</label>
                            <InputText id="linkTo" value={training.beplacementname} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !training.beplacementname })} />

                        </div>
                    </Dialog>

                    <Dialog visible={deletePlacedDialog} style={{ width: '450px' }} header="Confirm" modal footer={deletePlacedDialogFooter} onHide={hidedeletePlacedDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {training && <span>Are you sure you want to delete <b>{training.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteTrainingDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteTrainingDialogFooter} onHide={hidedeleteTrainingDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {training && <span>Are you sure you want to delete the selected placed?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(TrainingCell, comparisonFn);
import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Training } from '../../service/Training/Training';


const StudentRecordChart = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyEvents = {
        id: null,
        stId: null,
        statisticsImg:''
    };

    const [record, setRecord] = useState(null);
    const [recordDialog, setRecordDialog] = useState(false);
    const [deleteRecordDialog, setDeleteRecordDialog] = useState(false);
    const [deleteStudentRecDialog, setDeleteStudentRecDialog] = useState(false);
    const [studentRec, setStudentRec] = useState(emptyEvents);
    const [selectedRecord, setSelectedRecord] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const training = new Training();
        training.getStudentRecord().then(data => setRecord(data));
    }, []);


    const openNew = () => {
        setStudentRec(emptyEvents);
        setSubmitted(false);
        setRecordDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setRecordDialog(false);
    }

    const hidedeleteRecordDialog = () => {
        setDeleteRecordDialog(false);
    }

    const hidedeleteStudentRecDialog = () => {
        setDeleteStudentRecDialog(false);
    }

    const savestudentRec = () => {
        setSubmitted(true);

        if (studentRec.name.trim()) {
            let _record = [...record];
            let _studentRec = { ...studentRec };
            if (studentRec.id) {
                const index = findIndexById(studentRec.id);

                _record[index] = _studentRec;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'studentRec Updated', life: 3000 });
            }
            else {
                _studentRec.id = createId();
                _studentRec.image = 'studentRec-placeholder.svg';
                _record.push(_studentRec);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'studentRec Created', life: 3000 });
            }

            setRecord(_record);
            setRecordDialog(false);
            setStudentRec(emptyEvents);
        }
    }

    const editstudentRec = (studentRec) => {
        setStudentRec({ ...studentRec });
        setRecordDialog(true);
    }

    const confirmDeletestudentRec = (studentRec) => {
        setStudentRec(studentRec);
        setDeleteRecordDialog(true);
    }

    const deletestudentRec = () => {
        let _record = record.filter(val => val.id !== studentRec.id);
        setRecord(_record);
        setDeleteRecordDialog(false);
        setStudentRec(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'studentRec Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < record.length; i++) {
            if (record[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteStudentRecDialog(true);
    }

    const deleteselectedRecord = () => {
        let _record = record.filter(val => !selectedRecord.includes(val));
        setRecord(_record);
        setDeleteStudentRecDialog(false);
        setSelectedRecord(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'record Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _studentRec = { ...studentRec };
        _studentRec[`${name}`] = val;
        setStudentRec(_studentRec);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedRecord || !selectedRecord.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             {rowData.stulistimg}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/statistics/${rowData.statisticsImg}`} alt={rowData.statisticsImg} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editstudentRec(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletestudentRec(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Student Placement Record</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const recordDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savestudentRec} />
        </>
    );
    const deleteRecordDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteRecordDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletestudentRec} />
        </>
    );
    const deleteStudentRecDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteStudentRecDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedRecord} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={record} selection={selectedRecord} onSelectionChange={(e) => setSelectedRecord(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} record"
                        globalFilter={globalFilter} emptyMessage="No record found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Record Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={recordDialog} style={{ width: '450px' }} header="Record" modal className="p-fluid" footer={recordDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteRecordDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteRecordDialogFooter} onHide={hidedeleteRecordDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {studentRec && <span>Are you sure you want to delete <b>{studentRec.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteStudentRecDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteStudentRecDialogFooter} onHide={hidedeleteStudentRecDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {studentRec && <span>Are you sure you want to delete the selected record?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(StudentRecordChart, comparisonFn);
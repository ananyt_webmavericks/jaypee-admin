import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { About } from '../../service/About/About';

const Technician = () => {


    let emptytechnos = {
        TechnicianId: null,
        id: null,
        Technicianname: '',
        Techniciantitle: '',
    };

    const [technician, setTechnician] = useState(null);
    const [techDialog, setTechDialog] = useState(false);
    const [deleteTechDialog, setDeleteTechDialog] = useState(false);
    const [deleteTechnicianDialog, setDeleteTechnicianDialog] = useState(false);
    const [technos, setTechnos] = useState(emptytechnos);
    const [selectedTechnician, setSelectedTechnician] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const about = new About();
        about.getTechnician().then(data => setTechnician(data));
    }, []);

    const openNew = () => {
        setTechnos(emptytechnos);
        setSubmitted(false);
        setTechDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setTechDialog(false);
    }

    const hidedeleteTechDialog = () => {
        setDeleteTechDialog(false);
    }

    const hidedeleteTechnicianDialog = () => {
        setDeleteTechnicianDialog(false);
    }

    const savetechnos = () => {
        setSubmitted(true);

        if (technos.name.trim()) {
            let _technician = [...technician];
            let _technos = { ...technos };
            if (technos.id) {
                const index = findIndexById(technos.id);

                _technician[index] = _technos;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'technos Updated', life: 3000 });
            }
            else {
                _technos.id = createId();
                _technos.image = 'technos-placeholder.svg';
                _technician.push(_technos);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'technos Created', life: 3000 });
            }

            setTechnician(_technician);
            setTechDialog(false);
            setTechnos(emptytechnos);
        }
    }

    const edittechnos = (technos) => {
        setTechnos({ ...technos });
        setTechDialog(true);
    }

    const confirmDeletetechnos = (technos) => {
        setTechnos(technos);
        setDeleteTechDialog(true);
    }

    const deletetechnos = () => {
        let _technician = technician.filter(val => val.id !== technos.id);
        setTechnician(_technician);
        setDeleteTechDialog(false);
        setTechnos(emptytechnos);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'technos Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < technician.length; i++) {
            if (technician[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteTechnicianDialog(true);
    }

    const deleteselectedTechnician = () => {
        let _technician = technician.filter(val => !selectedTechnician.includes(val));
        setTechnician(_technician);
        setDeleteTechnicianDialog(false);
        setSelectedTechnician(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'technician Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _technos = { ...technos };
        _technos[`${name}`] = val;
        setTechnos(_technos);
    }
    const onUploadChange = (e, image) => {
        const val = (e.target && e.target.value) || '';
        let _technos = { ...technos };
        _technos[`${image}`] = val;
        setTechnos(_technos);
    }


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedTechnician || !selectedTechnician.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" />
                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const idBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    // const CategoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Category</span>
    //             {rowData.afsheading}
    //         </>
    //     );
    // }
  

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }
    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.Technicianname}
            </>
        );
    }


    const titleBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">title</span>
                {rowData.Techniciantitle}
            </>
        );
    }

    // const positionBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Position</span>
    //           {rowData.afspostion}
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => edittechnos(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletetechnos(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">technician Staff</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const techDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savetechnos} />
        </>
    );
    const deleteTechDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteTechDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletetechnos} />
        </>
    );
    const deleteTechnicianDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteTechnicianDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedTechnician} />
        </>
    );

    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={technician} selection={selectedTechnician} onSelectionChange={(e) => setSelectedTechnician(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} technician"
                        globalFilter={globalFilter} emptyMessage="No technician found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem'}}></Column>
                        <Column field="technosistration Id" header="technosistration Id" sortable body={idBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Category" header="Category" sortable body={CategoryBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column> */}
                        <Column field="Name" header="Name" body={nameBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Title" header="Title" body={titleBodyTemplate} sortable headerStyle={{ width: '40%', minWidth: '8rem' }}></Column>
                        {/* <Column field="Position" header="Position" sortable body={positionBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column> */}
                       
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate}></Column>
                    </DataTable>

                    <Dialog visible={techDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={techDialogFooter} onHide={hideDialog}>
                        
                        <div className="field">
                        {technos.image && <img src={`assets/demo/images/staff/${technos.image}`} alt={technos.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}
                        <FileUpload mode="basic" onChange={(e) => onUploadChange(e, 'image')} accept="image/*" maxFileSize={1000000} label="Upload Image" chooseLabel="Upload Image" className="mr-2 inline-block"/>
                        </div>
                        <div className="field">
                            <label htmlFor="name">Name</label>
                            <InputText id="name" value={technos.name} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !technos.name })} />
                            {submitted && !technos.name && <small className="p-invalid">Name is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">Title</label>
                            <InputText id="title" value={technos.title} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !technos.title })} />
                            {submitted && !technos.title && <small className="p-invalid">Title is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="department">Department</label>
                            <InputText id="department" value={technos.dpmt} onChange={(e) => onInputChange(e, 'department')} required autoFocus className={classNames({ 'p-invalid': submitted && !technos.dpmt })} />
                            {submitted && !technos.dpmt && <small className="p-invalid">Department is required.</small>}
                        </div>

                    </Dialog>

                    <Dialog visible={deleteTechDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteTechDialogFooter} onHide={hidedeleteTechDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {technos && <span>Are you sure you want to delete <b>{technos.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteTechnicianDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteTechnicianDialogFooter} onHide={hidedeleteTechnicianDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {technos && <span>Are you sure you want to delete the selected technician?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(Technician, comparisonFn);
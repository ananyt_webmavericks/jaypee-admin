import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { About } from '../../service/About/About';

const AccountFinance = () => {


    let emptyfinance = {
        afsid: null,
        id: null,
        afsheading: '',
        afsname: '',
        afsdepartment: '',
        gaspostion: '',
    };

    const [account, setAccount] = useState(null);
    const [accDialog, setAccDialog] = useState(false);
    const [deleteAccDialog, setDeleteAccDialog] = useState(false);
    const [deleteAccountDialog, setDeleteAccountDialog] = useState(false);
    const [finance, setFinance] = useState(emptyfinance);
    const [selectedAccount, setSelectedAccount] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const about = new About();
        about.getAccount().then(data => setAccount(data));
    }, []);

    const openNew = () => {
        setFinance(emptyfinance);
        setSubmitted(false);
        setAccDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setAccDialog(false);
    }

    const hidedeleteAccDialog = () => {
        setDeleteAccDialog(false);
    }

    const hidedeleteAccountDialog = () => {
        setDeleteAccountDialog(false);
    }

    const savefinance = () => {
        setSubmitted(true);

        if (finance.name.trim()) {
            let _account = [...account];
            let _finance = { ...finance };
            if (finance.id) {
                const index = findIndexById(finance.id);

                _account[index] = _finance;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'finance Updated', life: 3000 });
            }
            else {
                _finance.id = createId();
                _finance.image = 'finance-placeholder.svg';
                _account.push(_finance);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'finance Created', life: 3000 });
            }

            setAccount(_account);
            setAccDialog(false);
            setFinance(emptyfinance);
        }
    }

    const editfinance = (finance) => {
        setFinance({ ...finance });
        setAccDialog(true);
    }

    const confirmDeletefinance = (finance) => {
        setFinance(finance);
        setDeleteAccDialog(true);
    }

    const deletefinance = () => {
        let _account = account.filter(val => val.id !== finance.id);
        setAccount(_account);
        setDeleteAccDialog(false);
        setFinance(emptyfinance);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'finance Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < account.length; i++) {
            if (account[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteAccountDialog(true);
    }

    const deleteselectedAccount = () => {
        let _account = account.filter(val => !selectedAccount.includes(val));
        setAccount(_account);
        setDeleteAccountDialog(false);
        setSelectedAccount(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'account Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _finance = { ...finance };
        _finance[`${name}`] = val;
        setFinance(_finance);
    }
    const onUploadChange = (e, image) => {
        const val = (e.target && e.target.value) || '';
        let _finance = { ...finance };
        _finance[`${image}`] = val;
        setFinance(_finance);
    }


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedAccount || !selectedAccount.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" />
                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const idBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Staff Id</span>
                {rowData.id}
            </>
        );
    }

    const CategoryBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Category</span>
                {rowData.afsheading}
            </>
        );
    }
  

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }
    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.afsname}
            </>
        );
    }


    const departmentBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.afsdepartment}
            </>
        );
    }

    const positionBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Position</span>
              {rowData.afspostion}
            </>
        );
    }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editfinance(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletefinance(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Manage Technical Staff</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const accDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savefinance} />
        </>
    );
    const deleteAccDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteAccDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletefinance} />
        </>
    );
    const deleteAccountDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteAccountDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedAccount} />
        </>
    );

    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={account} selection={selectedAccount} onSelectionChange={(e) => setSelectedAccount(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} account"
                        globalFilter={globalFilter} emptyMessage="No account found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem'}}></Column>
                        <Column field="financeistration Id" header="financeistration Id" sortable body={idBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Category" header="Category" sortable body={CategoryBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Name" header="Name" body={nameBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Department" header="Department" body={departmentBodyTemplate} sortable headerStyle={{ width: '20%', minWidth: '8rem' }}></Column>
                        <Column field="Position" header="Position" sortable body={positionBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                       
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate}></Column>
                    </DataTable>

                    <Dialog visible={accDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={accDialogFooter} onHide={hideDialog}>
                        
                        <div className="field">
                        {finance.image && <img src={`assets/demo/images/staff/${finance.image}`} alt={finance.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}
                        <FileUpload mode="basic" onChange={(e) => onUploadChange(e, 'image')} accept="image/*" maxFileSize={1000000} label="Upload Image" chooseLabel="Upload Image" className="mr-2 inline-block"/>
                        </div>
                        <div className="field">
                            <label htmlFor="name">Name</label>
                            <InputText id="name" value={finance.name} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !finance.name })} />
                            {submitted && !finance.name && <small className="p-invalid">Name is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">Title</label>
                            <InputText id="title" value={finance.title} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !finance.title })} />
                            {submitted && !finance.title && <small className="p-invalid">Title is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="department">Department</label>
                            <InputText id="department" value={finance.dpmt} onChange={(e) => onInputChange(e, 'department')} required autoFocus className={classNames({ 'p-invalid': submitted && !finance.dpmt })} />
                            {submitted && !finance.dpmt && <small className="p-invalid">Department is required.</small>}
                        </div>

                    </Dialog>

                    <Dialog visible={deleteAccDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteAccDialogFooter} onHide={hidedeleteAccDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {finance && <span>Are you sure you want to delete <b>{finance.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteAccountDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteAccountDialogFooter} onHide={hidedeleteAccountDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {finance && <span>Are you sure you want to delete the selected account?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(AccountFinance, comparisonFn);
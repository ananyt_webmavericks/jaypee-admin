import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { About } from '../../service/About/About';

const LrcStaff = () => {


    let emptySrLrc = {
        LrcId: null,
        id: null,
        Lrcname: '',
        Lrctitle: '',
    };

    const [staffLrc, setStaffLrc] = useState(null);
    const [lrcDialog, setLrcDialog] = useState(false);
    const [deleteLrcDialog, setDeleteLrcDialog] = useState(false);
    const [deleteStaffLrcDialog, setDeleteStaffLrcDialog] = useState(false);
    const [srLrc, setSrLrc] = useState(emptySrLrc);
    const [selectedStaffLrc, setSelectedStaffLrc] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const about = new About();
        about.getLrc().then(data => setStaffLrc(data));
    }, []);

    const openNew = () => {
        setSrLrc(emptySrLrc);
        setSubmitted(false);
        setLrcDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setLrcDialog(false);
    }

    const hidedeleteLrcDialog = () => {
        setDeleteLrcDialog(false);
    }

    const hidedeleteStaffLrcDialog = () => {
        setDeleteStaffLrcDialog(false);
    }

    const savesrLrc = () => {
        setSubmitted(true);

        if (srLrc.name.trim()) {
            let _staffLrc = [...staffLrc];
            let _srLrc = { ...srLrc };
            if (srLrc.id) {
                const index = findIndexById(srLrc.id);

                _staffLrc[index] = _srLrc;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'srLrc Updated', life: 3000 });
            }
            else {
                _srLrc.id = createId();
                _srLrc.image = 'srLrc-placeholder.svg';
                _staffLrc.push(_srLrc);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'srLrc Created', life: 3000 });
            }

            setStaffLrc(_staffLrc);
            setLrcDialog(false);
            setSrLrc(emptySrLrc);
        }
    }

    const editsrLrc = (srLrc) => {
        setSrLrc({ ...srLrc });
        setLrcDialog(true);
    }

    const confirmDeletesrLrc = (srLrc) => {
        setSrLrc(srLrc);
        setDeleteLrcDialog(true);
    }

    const deletesrLrc = () => {
        let _staffLrc = staffLrc.filter(val => val.id !== srLrc.id);
        setStaffLrc(_staffLrc);
        setDeleteLrcDialog(false);
        setSrLrc(emptySrLrc);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'srLrc Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < staffLrc.length; i++) {
            if (staffLrc[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteStaffLrcDialog(true);
    }

    const deleteselectedStaffLrc = () => {
        let _staffLrc = staffLrc.filter(val => !selectedStaffLrc.includes(val));
        setStaffLrc(_staffLrc);
        setDeleteStaffLrcDialog(false);
        setSelectedStaffLrc(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'staffLrc Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _srLrc = { ...srLrc };
        _srLrc[`${name}`] = val;
        setSrLrc(_srLrc);
    }
    const onUploadChange = (e, image) => {
        const val = (e.target && e.target.value) || '';
        let _srLrc = { ...srLrc };
        _srLrc[`${image}`] = val;
        setSrLrc(_srLrc);
    }


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedStaffLrc || !selectedStaffLrc.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" />
                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const idBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    // const CategoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Category</span>
    //             {rowData.afsheading}
    //         </>
    //     );
    // }
  

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }
    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.Lrcname}
            </>
        );
    }


    const titleBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">title</span>
                {rowData.Lrctitle}
            </>
        );
    }

    // const positionBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Position</span>
    //           {rowData.afspostion}
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editsrLrc(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletesrLrc(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">staffLrc Staff</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const lrcDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savesrLrc} />
        </>
    );
    const deleteLrcDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteLrcDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletesrLrc} />
        </>
    );
    const deleteStaffLrcDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteStaffLrcDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedStaffLrc} />
        </>
    );

    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={staffLrc} selection={selectedStaffLrc} onSelectionChange={(e) => setSelectedStaffLrc(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} staffLrc"
                        globalFilter={globalFilter} emptyMessage="No staffLrc found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem'}}></Column>
                        <Column field="srLrcistration Id" header="srLrcistration Id" sortable body={idBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Category" header="Category" sortable body={CategoryBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column> */}
                        <Column field="Name" header="Name" body={nameBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Title" header="Title" body={titleBodyTemplate} sortable headerStyle={{ width: '40%', minWidth: '8rem' }}></Column>
                        {/* <Column field="Position" header="Position" sortable body={positionBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column> */}
                       
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate}></Column>
                    </DataTable>

                    <Dialog visible={lrcDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={lrcDialogFooter} onHide={hideDialog}>
                        
                        <div className="field">
                        {srLrc.image && <img src={`assets/demo/images/staff/${srLrc.image}`} alt={srLrc.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}
                        <FileUpload mode="basic" onChange={(e) => onUploadChange(e, 'image')} accept="image/*" maxFileSize={1000000} label="Upload Image" chooseLabel="Upload Image" className="mr-2 inline-block"/>
                        </div>
                        <div className="field">
                            <label htmlFor="name">Name</label>
                            <InputText id="name" value={srLrc.name} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !srLrc.name })} />
                            {submitted && !srLrc.name && <small className="p-invalid">Name is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">Title</label>
                            <InputText id="title" value={srLrc.title} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !srLrc.title })} />
                            {submitted && !srLrc.title && <small className="p-invalid">Title is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="department">Department</label>
                            <InputText id="department" value={srLrc.dpmt} onChange={(e) => onInputChange(e, 'department')} required autoFocus className={classNames({ 'p-invalid': submitted && !srLrc.dpmt })} />
                            {submitted && !srLrc.dpmt && <small className="p-invalid">Department is required.</small>}
                        </div>

                    </Dialog>

                    <Dialog visible={deleteLrcDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteLrcDialogFooter} onHide={hidedeleteLrcDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {srLrc && <span>Are you sure you want to delete <b>{srLrc.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteStaffLrcDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteStaffLrcDialogFooter} onHide={hidedeleteStaffLrcDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {srLrc && <span>Are you sure you want to delete the selected staffLrc?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(LrcStaff, comparisonFn);
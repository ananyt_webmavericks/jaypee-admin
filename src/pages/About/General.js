import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { About } from '../../service/About/About';

const GeneralAdmin = () => {


    let emptyadmin = {
        gasid: null,
        id: null,
        gasheading: '',
        gasname: '',
        gasdepartment: '',
        gaspostion: '',
    };

    const [general, setgeneral] = useState(null);
    const [genDialog, setGenDialog] = useState(false);
    const [deleteGenDialog, setDeleteGenDialog] = useState(false);
    const [deleteGeneralDialog, setDeleteGeneralDialog] = useState(false);
    const [admin, setAdmin] = useState(emptyadmin);
    const [selectedGeneral, setSelectedGeneral] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const about = new About();
        about.getGeneral().then(data => setgeneral(data));
    }, []);

    const openNew = () => {
        setAdmin(emptyadmin);
        setSubmitted(false);
        setGenDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setGenDialog(false);
    }

    const hidedeleteGenDialog = () => {
        setDeleteGenDialog(false);
    }

    const hidedeleteGeneralDialog = () => {
        setDeleteGeneralDialog(false);
    }

    const saveadmin = () => {
        setSubmitted(true);

        if (admin.name.trim()) {
            let _general = [...general];
            let _admin = { ...admin };
            if (admin.id) {
                const index = findIndexById(admin.id);

                _general[index] = _admin;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'admin Updated', life: 3000 });
            }
            else {
                _admin.id = createId();
                _admin.image = 'admin-placeholder.svg';
                _general.push(_admin);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'admin Created', life: 3000 });
            }

            setgeneral(_general);
            setGenDialog(false);
            setAdmin(emptyadmin);
        }
    }

    const editadmin = (admin) => {
        setAdmin({ ...admin });
        setGenDialog(true);
    }

    const confirmDeleteadmin = (admin) => {
        setAdmin(admin);
        setDeleteGenDialog(true);
    }

    const deleteadmin = () => {
        let _general = general.filter(val => val.id !== admin.id);
        setgeneral(_general);
        setDeleteGenDialog(false);
        setAdmin(emptyadmin);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'admin Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < general.length; i++) {
            if (general[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteGeneralDialog(true);
    }

    const deleteselectedGeneral = () => {
        let _general = general.filter(val => !selectedGeneral.includes(val));
        setgeneral(_general);
        setDeleteGeneralDialog(false);
        setSelectedGeneral(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'general Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _admin = { ...admin };
        _admin[`${name}`] = val;
        setAdmin(_admin);
    }
    const onUploadChange = (e, image) => {
        const val = (e.target && e.target.value) || '';
        let _admin = { ...admin };
        _admin[`${image}`] = val;
        setAdmin(_admin);
    }


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedGeneral || !selectedGeneral.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" />
                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const idBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Staff Id</span>
                {rowData.id}
            </>
        );
    }

    const CategoryBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Category</span>
                {rowData.gasheading}
            </>
        );
    }
  

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }
    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.gasname}
            </>
        );
    }


    const departmentBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.gasdepartment}
            </>
        );
    }

    const positionBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Position</span>
              {rowData.gaspostion}
            </>
        );
    }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editadmin(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteadmin(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Manage Technical Staff</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const genDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveadmin} />
        </>
    );
    const deleteGenDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteGenDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteadmin} />
        </>
    );
    const deleteGeneralDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteGeneralDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedGeneral} />
        </>
    );

    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={general} selection={selectedGeneral} onSelectionChange={(e) => setSelectedGeneral(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} general"
                        globalFilter={globalFilter} emptyMessage="No general found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem'}}></Column>
                        <Column field="Administration Id" header="Administration Id" sortable body={idBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Category" header="Category" sortable body={CategoryBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Name" header="Name" body={nameBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Department" header="Department" body={departmentBodyTemplate} sortable headerStyle={{ width: '20%', minWidth: '8rem' }}></Column>
                        <Column field="Position" header="Position" sortable body={positionBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                       
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate}></Column>
                    </DataTable>

                    <Dialog visible={genDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={genDialogFooter} onHide={hideDialog}>
                        
                        <div className="field">
                        {admin.image && <img src={`assets/demo/images/staff/${admin.image}`} alt={admin.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}
                        <FileUpload mode="basic" onChange={(e) => onUploadChange(e, 'image')} accept="image/*" maxFileSize={1000000} label="Upload Image" chooseLabel="Upload Image" className="mr-2 inline-block"/>
                        </div>
                        <div className="field">
                            <label htmlFor="name">Name</label>
                            <InputText id="name" value={admin.name} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !admin.name })} />
                            {submitted && !admin.name && <small className="p-invalid">Name is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">Title</label>
                            <InputText id="title" value={admin.title} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !admin.title })} />
                            {submitted && !admin.title && <small className="p-invalid">Title is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="department">Department</label>
                            <InputText id="department" value={admin.dpmt} onChange={(e) => onInputChange(e, 'department')} required autoFocus className={classNames({ 'p-invalid': submitted && !admin.dpmt })} />
                            {submitted && !admin.dpmt && <small className="p-invalid">Department is required.</small>}
                        </div>

                    </Dialog>

                    <Dialog visible={deleteGenDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteGenDialogFooter} onHide={hidedeleteGenDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {admin && <span>Are you sure you want to delete <b>{admin.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteGeneralDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteGeneralDialogFooter} onHide={hidedeleteGeneralDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {admin && <span>Are you sure you want to delete the selected general?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(GeneralAdmin, comparisonFn);
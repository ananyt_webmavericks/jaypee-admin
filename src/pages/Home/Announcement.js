import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { UpcomingEvent } from '../../service/Home/Events';



const Announcement = () => {


    let emptyAnnouncementList = {
        id: null,
        announcement: '',
        linkTo: '',
    };

    const [announcementsList, setannouncementsList] = useState(null);
    const [announceDialog, setAnnounceDialog] = useState(false);
    const [deleteAnnounceDialog, setDeleteAnnounceDialog] = useState(false);
    const [deleteAnnouncementsListDialog, setDeleteAnnouncementsListDialog] = useState(false);
    const [announcementList, setAnnouncementList] = useState(emptyAnnouncementList);
    const [selectedAnnouncementsList, setSelectedAnnouncementsList] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const upcomingEvent = new UpcomingEvent();
        upcomingEvent.getAnnouncement().then(data => setannouncementsList(data));
    }, []);

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setAnnouncementList(emptyAnnouncementList);
        setSubmitted(false);
        setAnnounceDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setAnnounceDialog(false);
    }

    const hidedeleteAnnounceDialog = () => {
        setDeleteAnnounceDialog(false);
    }

    const hidedeleteAnnouncementsListDialog = () => {
        setDeleteAnnouncementsListDialog(false);
    }

    const saveannouncementList = () => {
        setSubmitted(true);

        if (announcementList.name.trim()) {
            let _announcementsList = [...announcementsList];
            let _announcementList = { ...announcementList };
            if (announcementList.id) {
                const index = findIndexById(announcementList.id);

                _announcementsList[index] = _announcementList;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'announcementList Updated', life: 3000 });
            }
            else {
                _announcementList.id = createId();
                _announcementList.image = 'announcementList-placeholder.svg';
                _announcementsList.push(_announcementList);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'announcementList Created', life: 3000 });
            }

            setannouncementsList(_announcementsList);
            setAnnounceDialog(false);
            setAnnouncementList(emptyAnnouncementList);
        }
    }

    const editannouncementList = (announcementList) => {
        setAnnouncementList({ ...announcementList });
        setAnnounceDialog(true);
    }

    const confirmDeleteannouncementList = (announcementList) => {
        setAnnouncementList(announcementList);
        setDeleteAnnounceDialog(true);
    }

    const deleteannouncementList = () => {
        let _announcementsList = announcementsList.filter(val => val.id !== announcementList.id);
        setannouncementsList(_announcementsList);
        setDeleteAnnounceDialog(false);
        setAnnouncementList(emptyAnnouncementList);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'announcementList Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < announcementsList.length; i++) {
            if (announcementsList[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteAnnouncementsListDialog(true);
    }

    const deleteselectedAnnouncementsList = () => {
        let _announcementsList = announcementsList.filter(val => !selectedAnnouncementsList.includes(val));
        setannouncementsList(_announcementsList);
        setDeleteAnnouncementsListDialog(false);
        setSelectedAnnouncementsList(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'announcementsList Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _announcementList = { ...announcementList };
        _announcementList[`${name}`] = val;
        setAnnouncementList(_announcementList);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedAnnouncementsList || !selectedAnnouncementsList.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Announcements</span>
                {rowData.announcement}
            </>
        );
    }

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">LinkTo</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }

    const priceBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">LinkTo</span>
                {formatCurrency(rowData.linkTo)}
            </>
        );
    }

    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Department</span>
    //             {rowData.dpmt}
    //         </>
    //     );
    // }

    // const ratingBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Readmore</span>
    //             <Rating value={rowData.readmore} readonly cancel={false} />
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editannouncementList(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteannouncementList(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Important Announcement List</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const announceDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveannouncementList} />
        </>
    );
    const deleteAnnounceDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteAnnounceDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteannouncementList} />
        </>
    );
    const deleteAnnouncementsListDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteAnnouncementsListDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedAnnouncementsList} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={announcementsList} selection={selectedAnnouncementsList} onSelectionChange={(e) => setSelectedAnnouncementsList(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} announcementsList"
                        globalFilter={globalFilter} emptyMessage="No announcementsList found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Announcements" header="Announcements" sortable body={nameBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Link TO" header="Link TO" body={priceBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate} style={{ float: "right",  paddingBottom: "22px" ,width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={announceDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={announceDialogFooter} onHide={hideDialog}>
                    <div className="field">
                    <FileUpload  mode = "basic" accept="pdf/*"  label="Upload File" chooseLabel="Upload File" className="mr-2 inline-block"/>
 
                    </div>
                        <div className="field">
                            <label htmlFor="name">Announcement</label>
                            <InputTextarea id="announcement" value={announcementList.announcement} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !announcementList.announcement })} />
                            {submitted && !announcementList.name && <small className="p-invalid">Announcement is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">link To</label>
                            <InputText id="linkTo" value={announcementList.linkTo} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !announcementList.linkTo })} />

                        </div>

                    </Dialog>

                    <Dialog visible={deleteAnnounceDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteAnnounceDialogFooter} onHide={hidedeleteAnnounceDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {announcementList && <span>Are you sure you want to delete <b>{announcementList.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteAnnouncementsListDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteAnnouncementsListDialogFooter} onHide={hidedeleteAnnouncementsListDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {announcementList && <span>Are you sure you want to delete the selected announcementsList?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(Announcement, comparisonFn);
import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Facilities } from '../../service/Home/Facilities';


const FacilitiesClass = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyclasses = {
        id: null,
        classId:null,
        classImage:'',
    };

    const [classes, setClasses] = useState(null);
    const [classesDialog, setClassesDialog] = useState(false);
    const [deleteClassesDialog, setDeleteClassesDialog] = useState(false);
    const [deleteFacClassesDialog, setDeleteFacClassesDialog] = useState(false);
    const [facClasses, setFacClasses] = useState(emptyclasses);
    const [selectedClasses, setSelectedClasses] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const facilities = new Facilities();
        facilities.getFacilitiesClass().then(data => setClasses(data));
    }, []);


    const openNew = () => {
        setFacClasses(emptyclasses);
        setSubmitted(false);
        setClassesDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setClassesDialog(false);
    }

    const hidedeleteClassesDialog = () => {
        setDeleteClassesDialog(false);
    }

    const hidedeleteFacClassesDialog = () => {
        setDeleteFacClassesDialog(false);
    }

    const savefacClasses = () => {
        setSubmitted(true);

        if (facClasses.name.trim()) {
            let _classes = [...classes];
            let _facClasses = { ...facClasses };
            if (facClasses.id) {
                const index = findIndexById(facClasses.id);

                _classes[index] = _facClasses;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facClasses Updated', life: 3000 });
            }
            else {
                _facClasses.id = createId();
                _facClasses.image = 'facClasses-placeholder.svg';
                _classes.push(_facClasses);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facClasses Created', life: 3000 });
            }

            setClasses(_classes);
            setClassesDialog(false);
            setFacClasses(emptyclasses);
        }
    }

    const editfacClasses = (facClasses) => {
        setFacClasses({ ...facClasses });
        setClassesDialog(true);
    }

    const confirmDeletefacClasses = (facClasses) => {
        setFacClasses(facClasses);
        setDeleteClassesDialog(true);
    }

    const deletefacClasses = () => {
        let _classes = classes.filter(val => val.id !== facClasses.id);
        setClasses(_classes);
        setDeleteClassesDialog(false);
        setFacClasses(emptyclasses);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facClasses Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < classes.length; i++) {
            if (classes[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteFacClassesDialog(true);
    }

    const deleteselectedClasses = () => {
        let _classes = classes.filter(val => !selectedClasses.includes(val));
        setClasses(_classes);
        setDeleteFacClassesDialog(false);
        setSelectedClasses(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'classes Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _facClasses = { ...facClasses };
        _facClasses[`${name}`] = val;
        setFacClasses(_facClasses);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedClasses || !selectedClasses.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/class/${rowData.classImage}`} alt={rowData.classImage} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editfacClasses(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletefacClasses(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">classes Images</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const classesDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savefacClasses} />
        </>
    );
    const deleteClassesDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteClassesDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletefacClasses} />
        </>
    );
    const deleteFacClassesDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteFacClassesDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedClasses} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={classes} selection={selectedClasses} onSelectionChange={(e) => setSelectedClasses(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} classes"
                        globalFilter={globalFilter} emptyMessage="No classes found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={classesDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={classesDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteClassesDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteClassesDialogFooter} onHide={hidedeleteClassesDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facClasses && <span>Are you sure you want to delete <b>{facClasses.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteFacClassesDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteFacClassesDialogFooter} onHide={hidedeleteFacClassesDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facClasses && <span>Are you sure you want to delete the selected classes?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(FacilitiesClass, comparisonFn);
import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Infra } from '../../service/Home/Infra';


const Infrastructure = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyEvents = {
        id: null,
        Infrahomeid:null,
        Infrahomeimg:'',
    };

    const [infrastructs, setInfrastructs] = useState(null);
    const [infraDialog, setInfraDialog] = useState(false);
    const [deleteInfraDialog, setDeleteInfraDialog] = useState(false);
    const [deleteinfrastructfDialog, setDeleteinfrastructfDialog] = useState(false);
    const [infrastructf, setiInfrastructf] = useState(emptyEvents);
    const [selectedInfrastructs, setSelectedInfrastructs] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const infra = new Infra();
        infra.getInfra().then(data => setInfrastructs(data));
    }, []);


    const openNew = () => {
        setiInfrastructf(emptyEvents);
        setSubmitted(false);
        setInfraDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setInfraDialog(false);
    }

    const hidedeleteInfraDialog = () => {
        setDeleteInfraDialog(false);
    }

    const hidedeleteinfrastructfDialog = () => {
        setDeleteinfrastructfDialog(false);
    }

    const saveinfrastructf = () => {
        setSubmitted(true);

        if (infrastructf.name.trim()) {
            let _infrastructs = [...infrastructs];
            let _infrastructf = { ...infrastructf };
            if (infrastructf.id) {
                const index = findIndexById(infrastructf.id);

                _infrastructs[index] = _infrastructf;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'infrastructf Updated', life: 3000 });
            }
            else {
                _infrastructf.id = createId();
                _infrastructf.image = 'infrastructf-placeholder.svg';
                _infrastructs.push(_infrastructf);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'infrastructf Created', life: 3000 });
            }

            setInfrastructs(_infrastructs);
            setInfraDialog(false);
            setiInfrastructf(emptyEvents);
        }
    }

    const editinfrastructf = (infrastructf) => {
        setiInfrastructf({ ...infrastructf });
        setInfraDialog(true);
    }

    const confirmDeleteinfrastructf = (infrastructf) => {
        setiInfrastructf(infrastructf);
        setDeleteInfraDialog(true);
    }

    const deleteinfrastructf = () => {
        let _infrastructs = infrastructs.filter(val => val.id !== infrastructf.id);
        setInfrastructs(_infrastructs);
        setDeleteInfraDialog(false);
        setiInfrastructf(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'infrastructf Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < infrastructs.length; i++) {
            if (infrastructs[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteinfrastructfDialog(true);
    }

    const deleteselectedInfrastructs = () => {
        let _infrastructs = infrastructs.filter(val => !selectedInfrastructs.includes(val));
        setInfrastructs(_infrastructs);
        setDeleteinfrastructfDialog(false);
        setSelectedInfrastructs(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'infrastructs Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _infrastructf = { ...infrastructf };
        _infrastructf[`${name}`] = val;
        setiInfrastructf(_infrastructf);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedInfrastructs || !selectedInfrastructs.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/infra/${rowData.Infrahomeimg}`} alt={rowData.Infrahomeimg} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editinfrastructf(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteinfrastructf(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Infrastructure</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const infraDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveinfrastructf} />
        </>
    );
    const deleteInfraDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteInfraDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteinfrastructf} />
        </>
    );
    const deleteinfrastructfDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteinfrastructfDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedInfrastructs} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={infrastructs} selection={selectedInfrastructs} onSelectionChange={(e) => setSelectedInfrastructs(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} infrastructs"
                        globalFilter={globalFilter} emptyMessage="No infrastructs found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={infraDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={infraDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteInfraDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteInfraDialogFooter} onHide={hidedeleteInfraDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {infrastructf && <span>Are you sure you want to delete <b>{infrastructf.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteinfrastructfDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteinfrastructfDialogFooter} onHide={hidedeleteinfrastructfDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {infrastructf && <span>Are you sure you want to delete the selected infrastructs?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(Infrastructure, comparisonFn);
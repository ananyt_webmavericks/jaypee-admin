import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Facilities } from '../../service/Home/Facilities';


const FacilitiesLib = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptylibrary = {
        id: null,
        libraryId:null,
        libraryImage:'',
    };

    const [library, setLibrary] = useState(null);
    const [libraryDialog, setLibraryDialog] = useState(false);
    const [deleteLibraryDialog, setDeleteLibraryDialog] = useState(false);
    const [deleteFacLibraryDialog, setDeleteFacLibraryDialog] = useState(false);
    const [facLibrary, setFacLibrary] = useState(emptylibrary);
    const [selectedLibrary, setSelectedLibrary] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const facilities = new Facilities();
        facilities.getFacilitiesLib().then(data => setLibrary(data));
    }, []);


    const openNew = () => {
        setFacLibrary(emptylibrary);
        setSubmitted(false);
        setLibraryDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setLibraryDialog(false);
    }

    const hidedeleteLibraryDialog = () => {
        setDeleteLibraryDialog(false);
    }

    const hidedeleteFacLibraryDialog = () => {
        setDeleteFacLibraryDialog(false);
    }

    const savefacLibrary = () => {
        setSubmitted(true);

        if (facLibrary.name.trim()) {
            let _library = [...library];
            let _facLibrary = { ...facLibrary };
            if (facLibrary.id) {
                const index = findIndexById(facLibrary.id);

                _library[index] = _facLibrary;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facLibrary Updated', life: 3000 });
            }
            else {
                _facLibrary.id = createId();
                _facLibrary.image = 'facLibrary-placeholder.svg';
                _library.push(_facLibrary);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facLibrary Created', life: 3000 });
            }

            setLibrary(_library);
            setLibraryDialog(false);
            setFacLibrary(emptylibrary);
        }
    }

    const editfacLibrary = (facLibrary) => {
        setFacLibrary({ ...facLibrary });
        setLibraryDialog(true);
    }

    const confirmDeletefacLibrary = (facLibrary) => {
        setFacLibrary(facLibrary);
        setDeleteLibraryDialog(true);
    }

    const deletefacLibrary = () => {
        let _library = library.filter(val => val.id !== facLibrary.id);
        setLibrary(_library);
        setDeleteLibraryDialog(false);
        setFacLibrary(emptylibrary);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facLibrary Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < library.length; i++) {
            if (library[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteFacLibraryDialog(true);
    }

    const deleteselectedLibrary = () => {
        let _library = library.filter(val => !selectedLibrary.includes(val));
        setLibrary(_library);
        setDeleteFacLibraryDialog(false);
        setSelectedLibrary(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'library Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _facLibrary = { ...facLibrary };
        _facLibrary[`${name}`] = val;
        setFacLibrary(_facLibrary);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedLibrary || !selectedLibrary.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/library/${rowData.libraryImage}`} alt={rowData.libraryImage} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editfacLibrary(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletefacLibrary(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">library Images</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const libraryDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savefacLibrary} />
        </>
    );
    const deleteLibraryDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteLibraryDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletefacLibrary} />
        </>
    );
    const deleteFacLibraryDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteFacLibraryDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedLibrary} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={library} selection={selectedLibrary} onSelectionChange={(e) => setSelectedLibrary(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} library"
                        globalFilter={globalFilter} emptyMessage="No library found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={libraryDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={libraryDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteLibraryDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteLibraryDialogFooter} onHide={hidedeleteLibraryDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facLibrary && <span>Are you sure you want to delete <b>{facLibrary.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteFacLibraryDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteFacLibraryDialogFooter} onHide={hidedeleteFacLibraryDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facLibrary && <span>Are you sure you want to delete the selected library?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(FacilitiesLib, comparisonFn);
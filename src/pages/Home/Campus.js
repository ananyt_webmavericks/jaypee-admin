import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Camp } from '../../service/Home/Camp';


const Campus = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyCampus = {
        id: null,
        campushomeid:null,
        campushomeimg:'',
    };

    const [campus, setCampus] = useState(null);
    const [campusDialog, setCampusDialog] = useState(false);
    const [deleteCampusDialog, setDeleteCampusDialog] = useState(false);
    const [deleteCampimageDialog, setdeleteCampimageDialog] = useState(false);
    const [campimage, setCampimage] = useState(emptyCampus);
    const [selectedCampus, setSelectedCampus] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const camp = new Camp();
        camp.getCamp().then(data => setCampus(data));
    }, []);


    const openNew = () => {
        setCampimage(emptyCampus);
        setSubmitted(false);
        setCampusDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setCampusDialog(false);
    }

    const hidedeleteCampusDialog = () => {
        setDeleteCampusDialog(false);
    }

    const hidedeleteCampimageDialog = () => {
        setdeleteCampimageDialog(false);
    }

    const savecampimage = () => {
        setSubmitted(true);

        if (campimage.name.trim()) {
            let _campus = [...campus];
            let _campimage = { ...campimage };
            if (campimage.id) {
                const index = findIndexById(campimage.id);

                _campus[index] = _campimage;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'campimage Updated', life: 3000 });
            }
            else {
                _campimage.id = createId();
                _campimage.image = 'campimage-placeholder.svg';
                _campus.push(_campimage);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'campimage Created', life: 3000 });
            }

            setCampus(_campus);
            setCampusDialog(false);
            setCampimage(emptyCampus);
        }
    }

    const editcampimage = (campimage) => {
        setCampimage({ ...campimage });
        setCampusDialog(true);
    }

    const confirmDeletecampimage = (campimage) => {
        setCampimage(campimage);
        setDeleteCampusDialog(true);
    }

    const deletecampimage = () => {
        let _campus = campus.filter(val => val.id !== campimage.id);
        setCampus(_campus);
        setDeleteCampusDialog(false);
        setCampimage(emptyCampus);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'campimage Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < campus.length; i++) {
            if (campus[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setdeleteCampimageDialog(true);
    }

    const deleteselectedCampus = () => {
        let _campus = campus.filter(val => !selectedCampus.includes(val));
        setCampus(_campus);
        setdeleteCampimageDialog(false);
        setSelectedCampus(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'campus Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _campimage = { ...campimage };
        _campimage[`${name}`] = val;
        setCampimage(_campimage);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedCampus || !selectedCampus.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/campus/${rowData.campushomeimg}`} alt={rowData.campushomeimg} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editcampimage(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletecampimage(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Campus Images</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const campusDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savecampimage} />
        </>
    );
    const deleteCampusDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteCampusDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletecampimage} />
        </>
    );
    const deleteCampimageDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteCampimageDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedCampus} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={campus} selection={selectedCampus} onSelectionChange={(e) => setSelectedCampus(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} campus"
                        globalFilter={globalFilter} emptyMessage="No campus found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={campusDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={campusDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteCampusDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCampusDialogFooter} onHide={hidedeleteCampusDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {campimage && <span>Are you sure you want to delete <b>{campimage.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteCampimageDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCampimageDialogFooter} onHide={hidedeleteCampimageDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {campimage && <span>Are you sure you want to delete the selected campus?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(Campus, comparisonFn);